/* uart.h */
/* Simple driver for SERCOM USART peripheral in interrupt mode for Atmel SAM D2x devices. */
#ifndef UART_H
#define UART_H

#include <stddef.h>

/* Abstract data type that must be passed to every uart function */
typedef struct UART UART;

/* Obtain a pointer to a valid UART */
UART *uart_init(const char *name);
/* Configure callbacks */
void uart_set_callbacks(UART *uart, void (rx_callback)(UART *, unsigned char c), int (*tx_callback)(UART *), void (*txstart_callback)(UART *), void (*txend_callback)(UART *));
/* Configure buffers */
void uart_set_buffers(UART *uart, void *rxfifo, size_t rxsize, void *txfifo, size_t txsize);
/* Configure dummy bytes before starting transmitting */
void uart_set_txdummy(UART *uart, unsigned char dummy_num, unsigned char dummy_value);

void uart_set_noise(UART *uart, int lvl);

/* Open UART, previously initizialized */
int uart_open(UART *uart, unsigned int baudrate);
/* Close UART, previously open and initialized */
void uart_close(UART *uart);

/* Check if rx fifo is empty */
unsigned char uart_rxempty(UART *uart);

/* Read the next byte from UART (it blocks on rx fifo empty) */
unsigned char uart_getchar(UART *uart);

/* Check if the transmission isn't possible (tx fifo is full) */
unsigned char uart_txfull(UART *uart);

/* Transmit a byte (it blocks on tx fifo full) */
void uart_putchar(UART *uart, unsigned char c);

/* Read and reset the error flag */
int uart_error(UART *uart);

#endif
