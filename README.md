# bHDLC #

bHDLC is a very simple protocol for full-duplex point-to-point links. The project aims to be used in embedded devices with limited resources.

As the name implies, bHDLC protocol started as a simple implementation of [HDLC](https://en.wikipedia.org/wiki/High-Level_Data_Link_Control) in Asyncronous Balanced Mode (ABM) operational mode. The b first letter stands for "balanced".
However I have to admit bHDLC is vaguely inspired from HDLC. My goal wasn't implementing the HDLC standard in every detail. Maybe bHDLC name is wrong, because it remembers too much HDLC standard.

Many times I need to exchange data between two nodes over a full-duplex point-to-point links, typically UART or RS232 asyncronous links. The nodes can be two PCs, one embedded device and a PC or two embedded devices (MCUs). Each time I need to reinvent the wheel, designing a full protocol stack, starting from the link level to application level.

So I decided to separate the link layer (we don't have a real network layer over poin-to-point links) into a reusable library. Only the application layer should be written, depending on... the application.

## Error recovery ##
Most of the time, the links are almost error-free, but I don't think it's a good idea to design a protocol that doesn't work even with a single error bit.
The simplest strategy for error recovery is acks and retransmissions. If the ack is corrupted, the sender retransmits the frame.
However with this strategy, the receiver could receive **and process** the same frame two or more times. Depending on the application protocol, duplicated frames can be dangerous (suppose a message encapsulated in a frame that says "charge a bank account of 1000USD).
In order to detect duplicated frames, the sender needs to mark the frame with a sequence number (as in TCP or HDLC). In this way the duplicated frame is detected and discarded (the ack for that frame is send again).
If we introduce sequence numbers, you need a mechanism to syncronize the nodes so they agree on the same starting value, i.e. the protocol should provide a "connection" mechanism: immediately after a connection, the starting sequence number is zero.
Note that you need a sequence number for communication from A to B and a **different** sequence number for communication from B to A.

TCP has all those characteristics and could be used over serial links with SLIP protocol interface. I tried to use [lwip](http://savannah.nongnu.org/projects/lwip/) for full-duplex point-to-point links, but I immediately understood it's a too complex solution, even if lwip functionalities can be cut down. 
Another protocol with all those characteristics is HDLC and it seems simple to implement in embedded devices. I was very surprised to not find an already written HDLC implementation... so I started my implementation and bHDLC was born.

## Stream or message oriented ##
TCP is a stream-oriented protocol: the sender push bytes to the output queue and TCP guarantees the receiver will received the same ordered sequence of bytes. This means the reicever is forced to use an additional buffer to reconstruct the original message that could be received fragmented.

I'm not a fan of stream-oriented protocol in embedded devices, because of this additional buffer. So I designed bHDLC protocol as message-oriented protocol.
When you send a message with bHDLC_send(), you can be sure the receiver will receive the complete message with HDLC_EVENT_RECV (or nothing, if many errors occur). In this way, the receive doesn't need to use an additional buffer to reconstruct the original sender message.

Another good point for message-oriented protocol is that it can be used for stream-oriented application. Use a FIFO output buffer in the sender and a FIFO input buffer in the receiver. The sender pushes bytes to the output FIFO and, when you want (when the FIFO is full or you don't have additional bytes for many times) call bHDLC_send(). The receiver pushes bytes to the input FIFO when it receives a frame.

## How to use ##

Copy bHDLC source directory into your project and add all the source files (bHDLC.c and bHDLC_fcs.c) to your project.

Write your own bHDLC_conf_user.h file to customize some bHDLC features (see bHDLC_conf.h for explanations). This method is similar to lwipopts.h file for lwip project.

Write the serial device wrapper functions sio_init(), sio_putc() and sio_getc(). All of them use a serial device descriptor of type sio_fd_t to distinguish the serial port to associate to a bHDLC link (you can use bHDLC on multiple serial devices at the same time).

Write the function sys_now() that returns system ticks, usually milliseconds. This is used by bHDLC for managing timers.

Activate a bHDLC link with bHDLC_add(), bHDLC_set_rxbuf() and bHDLC_set_up() at the minimum. If you want, you can change the maximum number of retransmission (before disconnection) with bHDLC_set_maxretr() and the ack receiving timeout with bHDLC_set_acktout().

Write your callback function, passed to bHDLC_add(). Its prototype is:
```
#!c
size_t my_callback(bHDLC_link *link, bHDLC_Event event, const void *data, size_t size)
```
Write some code for the events you are interested in. Possible events are:
* HDLC_EVENT_CONNECTED, when the link has just connected (you could start a transmission, for example)
* HDLC_EVENT_DISCONNECTED, when the link has just disconnected (maybe the link is too noisy)
* HDLC_EVENT_RECV, frame received (look at data and size parameters)
* HDLC_EVENT_SENT, one frame was send and acked from the opposite host, look at data for the payload (we are sure the frame arrived at destination)
The return value is actually ignored.

If the node is like a client, i.e. it sends an explicit connection request to the opposite node, call bHDLC_connect() function. Otherwise, if it is like a server, it is already ready to accept incoming connection requests after bHDLC_set_up().

Call bHDLC_send() to push a message to the output queue. The message isn't transmitted immediately, so the payload buffer shouldn't be reused until HDLC_EVENT_SENT event with the same pointer is received.

In your mainloop, call bHDLC_poll().

## Supported platforms ##

All platforms where a standard C compiler is available. There are only two platform-dependent things to consider.

bHDLC code uses sio (serial I/O) hardware abstraction to model the serial device (as in SLIP/PPP implementation in lwip project). The user needs to write sio_init(), sio_getc() and sio_putc() functions.

bHDLC needs to manage a single timer error recovery mechanism (retransmission). The user needs to write sys_now() function that returns the current number of "ticks". A tick could be a millisecond, a microsecond or whatever you like. The timeout interval for ack reception is set as a multiple of system ticks.

## Examples ##

The code comes with a few examples that can be compiled with [mingw](http://www.mingw.org/). There's also a ready-to-use [Code::Blocks](http://www.codeblocks.org/) workspace/project.

All the examples share a simple serial device driver for COM port under Windows OS (I think it isn't a good piece of software, but it works for my goals and for testing the examples of bHDLC), and a simple software timers driver.

server_print example is very useful. It activates a bHDLC link and accepts any incoming connection request (it's a server). It prints to stdout all the frames received.

client_sendfile example shows how to send a file over bHDLC link.

The executable file accepts some command line options:
* -p<COM number>, to specify the COM port number
* -e<name>, to select the example to start
* -n<noise level>, to simulate some random errors on the link
* -t<timeout>, timeout (in ticks unit) for retransmission
* -r<max retries>, maximum number of retransmission for each output frame, before forced disconnection
After those main options, use -- (double hyphen) to add specific options for the example selected (for example, client_sendfile accepts the name of the file to send).

You can launch two bHDLC.exe instances over two different virtual COM ports. You can use [com0com](http://com0com.sourceforge.net/) to create a virtual null-modem connection between the two ports, so you can simulate the complete link on your Windows PC.