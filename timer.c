/* timer.c */
#include <stdio.h>
#include <sys/time.h>
#include "timer.h"

/* Prototipi di funzioni esterne */
void
timer_init(void)
{

}

int
timeval_subtract(struct timeval *result, struct timeval *x, struct timeval *y)
{
	/* Perform the carry for the later subtraction by updating y. */
	if (x->tv_usec < y->tv_usec) {
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_usec - y->tv_usec > 1000000) {
		int nsec = (x->tv_usec - y->tv_usec) / 1000000;
		y->tv_usec += 1000000 * nsec;
		y->tv_sec -= nsec;
	}

	/* Compute the time remaining to wait. tv_usec is certainly positive. */
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_usec = x->tv_usec - y->tv_usec;

	/* Return 1 if result is negative. */
	return x->tv_sec < y->tv_sec;
}

void
TimerSet(Timer *tmr, unsigned int ms)
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	tmr->tv_sec = tp.tv_sec + ms / 1000;
	tmr->tv_usec = tp.tv_usec + (ms % 1000) * 1000;
	if (tmr->tv_usec >= 1000000) {
		tmr->tv_sec++;
		tmr->tv_usec -= 1000000;
	}
}

int
TimerExpired(Timer *tmr)
{
	struct timeval tnow;
	struct timeval res;
	gettimeofday(&tnow, NULL);
	timeval_subtract(&res, &tnow, tmr);
	return res.tv_sec >= 0;
}

