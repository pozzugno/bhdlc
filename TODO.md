# bHDLC Todo list #

* arg member of bHDLC_link is now unused. Write bHDLC_set_arg() and bHDLC_get_arg() functions. It could be useful with multiple phisical/logical links active at the same time that needs to share the same callback function.

* Address is actually not used, even if it is transmitted in a byte. In the future, this byte could be used to create multiple virtual connections over a single physical link (the address transmitted in the frame will be the id of the virtual link).

* Implement DISC and DM U-Frames to disconnect a link in CONNECTED state. 
Note: IMHO it's not so useful. Actually the node that wants to disconnect calls bHDLC_close() that simply change the state to DISCONNECTED. Next received I- and S-frames will be silently discarded, so the opposite node automatically disconnects. However, if the opposite node doesn't send any frames, it will never know the peer has disconnected. 

* After activation, the link automatically accepts incoming connection requests. We can introduce a new CONNECTION_REQUEST event and accepts connection only if user callback confirms.

* When a I-frame is received, it is passed to the user callback with HDLC_EVENT_RECV event. Many times, the application needs to answer, but in some cases, it hasn't free space for pushing additional data to the output queue (for example because the sender sends too much frames together).
The user callback could return an error that says bHDLC to ignore (no ack) the received frame. In this case, the sender will send the frame again later and maybe the receiver will have free space to answer.
 