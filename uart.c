/* uart.c */
#include <stdio.h>
#include <stddef.h>
#include <windows.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "uart.h"
#include "process.h"
#include "qassert.h"

Q_DEFINE_THIS_MODULE("uart")

#define debug_printf			printf

/* Main uart data structure (defined here for encapsulation) */
struct UART {
	char name[6];
    HANDLE hUart;
	unsigned char open;

	/* Rx FIFO */
	unsigned char *rx_buff;
	size_t rx_size;
	volatile size_t rx_in;			/* Changed in interrupt */
	size_t rx_out;

	/* Tx FIFO */
	unsigned char *tx_buff;
	size_t tx_size;
	size_t tx_in;
	volatile size_t tx_out;			/* Changed in interrupt */

	/* Callbacks */
	void (*rx_callback)(UART *uart, unsigned char c);
	void (*txstart_callback)(UART *uart);
	void (*txend_callback)(UART *uart);
	int (*tx_callback)(UART *uart);

	int noise_lvl;
	unsigned char error;	/* Changed in interrupt */
};

/* Just a simple macro to manage FIFO roll-over */
#define ROLLOVER(x, max)					((x) + 1) >= (max) ? 0 : (x + 1)

void
rx_char(UART *uart, unsigned char c)
{
	if (uart->rx_callback) {
		/* Callback (ignore rx buffer) */
		uart->rx_callback(uart, c);
	} else {
		/* Manage rx buffer */
		size_t i = ROLLOVER(uart->rx_in, uart->rx_size);
		if(i == uart->rx_out) {
			return;			/* Rx FIFO full, discard character. */
		}
		uart->rx_buff[uart->rx_in] = c;
		uart->rx_in = i;
	}
}

void UartProc(void *arglist)
{
	UART *uart = (UART *)arglist;
	HANDLE hEvent;
	OVERLAPPED ol;
	hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if (hEvent == 0) {
		debug_printf("Error with CreateEvent\n");
		return;
	}

	while(1) {
		DWORD ret;
		unsigned char c;
		memset(&ol, 0, sizeof(ol));
		ol.hEvent = hEvent;
		ret = ReadFile(uart->hUart, &c, 1, NULL, &ol);
		if (ret != 0) {
			rx_char(uart, c);
		} else {
			int le = GetLastError();
			if (le == ERROR_IO_PENDING) {
				DWORD ret2;
				DWORD br;
				ret2 = GetOverlappedResult(uart->hUart, &ol, &br, TRUE);
				if(ret2 == 0) {
					debug_printf("error with GetOverlappedResult: %lu\n", ret2);
				} else {
					rx_char(uart, c);
				}

			} else {
				debug_printf("error with ReadFile() %d\n", le);
			}
		}
	}
	CloseHandle(hEvent);
	debug_printf("exiting from uart thread\n");
}

/* uart_init: initialize one UART and return pointer to use in additional API calls. */
UART *
uart_init(const char *name)
{
    Q_ASSERT(name != NULL);

    UART *uart = (UART *)malloc(sizeof(UART));
    if (uart != NULL) {
        snprintf(uart->name, sizeof(uart->name), name);
		uart->rx_callback = NULL;
		uart->tx_callback = NULL;
		uart->txstart_callback = NULL;
		uart->txend_callback = NULL;
		uart->rx_buff = NULL;
		uart->tx_buff = NULL;
		uart->open = 0;
    }
	return uart;
}

void
uart_set_callbacks(UART *uart, void (rx_callback)(UART *, unsigned char c), int (*tx_callback)(UART *), void (*txstart_callback)(UART *), void (*txend_callback)(UART *))
{
	uart->rx_callback = rx_callback;
	uart->tx_callback = tx_callback;
	uart->txstart_callback = txstart_callback;
	uart->txend_callback = txend_callback;
}

void
uart_set_buffers(UART *uart, void *rxfifo, size_t rxsize, void *txfifo, size_t txsize)
{
	uart->rx_buff = rxfifo;
	uart->rx_size = rxsize;
	uart->tx_buff = txfifo;
	uart->tx_size = txsize;
}

void
uart_set_txdummy(UART *uart, unsigned char dummy_num, unsigned char dummy_value)
{
    (void)uart;
    (void)dummy_num;
    (void)dummy_value;
}

void
uart_set_noise(UART *uart, int lvl)
{
	srand(time(NULL));
	if ((lvl >= 0) && (lvl <= 1000))
		uart->noise_lvl = lvl;
}

/* uart_open: open UART, starting receiving */
int
uart_open(UART *uart, unsigned int baudrate)
{
	/* Siccome non ho trovato una soluzione semplice per chiudere la porta seriale
	 * (considerando che c'� un thread in esecuzione), evito di chiudere veramente
	 * la porta e, quindi, evito di riaprirla se � gi� aperta. */
	if (uart->open) return -1;

    HANDLE hUart;

    DCB dcbSerialParams = {0};
    COMMTIMEOUTS timeouts={0};
    char uart_name[13];
    sprintf(uart_name, "\\\\.\\%s", uart->name);

	hUart = CreateFile(uart_name, GENERIC_READ | GENERIC_WRITE,
		0,
		0,
		OPEN_EXISTING,
		FILE_FLAG_OVERLAPPED,
		0
	);
    if (hUart != INVALID_HANDLE_VALUE) {
        dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
        if (!GetCommState(hUart, &dcbSerialParams)) {
             CloseHandle(hUart);
             return -1;
        }
        dcbSerialParams.fBinary = TRUE;
        dcbSerialParams.BaudRate = baudrate;
        dcbSerialParams.ByteSize = 8;
        dcbSerialParams.StopBits = ONESTOPBIT;
        dcbSerialParams.fParity = FALSE;
        dcbSerialParams.Parity = NOPARITY;
        dcbSerialParams.fDtrControl = DTR_CONTROL_ENABLE;
        dcbSerialParams.fRtsControl = RTS_CONTROL_DISABLE;
        dcbSerialParams.fOutxCtsFlow = FALSE;
        dcbSerialParams.fOutxDsrFlow = FALSE;
        dcbSerialParams.fDsrSensitivity = FALSE;
        dcbSerialParams.fOutX = FALSE;
        dcbSerialParams.fInX = FALSE;
        if(!SetCommState(hUart, &dcbSerialParams)){
			CloseHandle(hUart);
			return -1;
        }
        timeouts.ReadIntervalTimeout = 0;
        timeouts.ReadTotalTimeoutConstant = 0;
        timeouts.ReadTotalTimeoutMultiplier = 0;
        timeouts.WriteTotalTimeoutConstant = 0;
        timeouts.WriteTotalTimeoutMultiplier = 0;
        if(!SetCommTimeouts(hUart, &timeouts)){
			CloseHandle(hUart);
			return -1;
        }
    } else {
    	printf("Error with CreateFile: %lu\n", GetLastError());
        Q_ERROR();
		return -1;
    }
    uart->hUart = hUart;
	uart->open = 1;

	/* Reset FIFO buffers */
	uart->rx_in = uart->rx_out = 0;
	uart->tx_in = uart->tx_out = 0;

	_beginthread(UartProc, 0, uart);
    return 0;
}

/* uart_close: close UART */
void
uart_close(UART *uart)
{
}

/* uart_rxempty: return true if rx FIFO is empty */
unsigned char
uart_rxempty(UART *uart)
{
	return uart->rx_out == uart->rx_in;
}

/* uart_getchar: return the next received character. If rx FIFO is empty, it blocks. */
unsigned char
uart_getchar(UART *uart)
{
	unsigned char data;

	while(uart_rxempty(uart))
		;
	data = uart->rx_buff[uart->rx_out];
	uart->rx_out = ROLLOVER(uart->rx_out, uart->rx_size);
	return data;
}

/* uart_txfull: return true if tx FIFO is full */
unsigned char
uart_txfull(UART *uart)
{
	(void)uart;
    return 0;
}

/* uart_putchar: send a character. If tx FIFO is full, it blocks. */
void
uart_putchar(UART *uart, unsigned char c)
{
	/* Randomize character (noise) */
	if (uart->noise_lvl > 0) {
		double r = rand();
		double thres = (double)uart->noise_lvl * RAND_MAX / 1000;
		if (r < thres) {
			c = ~c;
			printf("*");
		}
	}

	HANDLE hEvent;
	OVERLAPPED ol;
	hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if (hEvent == 0) {
		debug_printf("Error with CreateEvent\n");
		return;
	}
	memset(&ol, 0, sizeof(ol));
	ol.hEvent = hEvent;

	DWORD dwBytesWritten = 0;
	int ret;
	ret = WriteFile(uart->hUart, &c, 1, &dwBytesWritten, &ol);
	if (ret != 0) {
		if (dwBytesWritten != 1) debug_printf("error with WriteFile\n");
	} else if (GetLastError() == ERROR_IO_PENDING) {
		while(GetOverlappedResult(uart->hUart, &ol, &dwBytesWritten, TRUE) == 0) {
			if (dwBytesWritten != 1) debug_printf("error with WriteFile\n");
		}
	} else {
		debug_printf("error in uart_putchar() %lu\n", GetLastError());
	}
	CloseHandle(hEvent);
}

/* uart_error: return and reset the error flag */
int
uart_error(UART *uart)
{
	int ret = uart->error;
	uart->error = 0;
	return ret;
}
