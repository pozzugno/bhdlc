#ifndef BHDLC_CONF_USER_H
#define BHDLC_CONF_USER_H

#define BHDLC_DEBUG				0

#define BHDLC_RETRY_MAX			3

#define BHDLC_SYS_TICKS_T		unsigned long
#define BHDLC_WAITACK_TIMEOUT	100		/* In ticks time unit (ms) */

#define BHDLC_TXWINDOW_SIZE		3
#define BHDLC_TXQUEUE_SIZE		5

#define BHDLC_STATISTICS		0

#endif
