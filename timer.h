/* timer.h */
#ifndef TIMER_H
#define TIMER_H

#include <limits.h>
#include <sys/time.h>

typedef struct timeval Timer;

void timer_init(void);
void TimerSet(Timer *tmr, unsigned int ms);
int TimerExpired(Timer *tmr);

#endif
