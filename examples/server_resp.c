#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <stdint.h>
#include <time.h>
#include "bHDLC/bHDLC.h"
#include "timer.h"
#include "qassert.h"

Q_DEFINE_THIS_MODULE("server2")

static unsigned char bHDLC_rxbuf[60 + BHDLC_FRAME_OVERHEAD];
static char bHDLC_txbuf[256];
static size_t txbuf_cnt;
static size_t txbuf_in;
static unsigned char txbuf_blocked;

static unsigned char connected = 0;

size_t
server_resp_cb(bHDLC_link *link, bHDLC_Event event, void *data, size_t size)
{
	switch(event) {
		case HDLC_EVENT_CONNECTED:
			printf("Connected\n");
			connected = 1;
			txbuf_cnt = txbuf_in = 0;
			txbuf_blocked = 0;
			return 0;
		case HDLC_EVENT_DISCONNECTED:
			printf("Disconnected\n");
			connected = 0;
			return 0;
		case HDLC_EVENT_RECV: {
			if (!txbuf_blocked) {
				int n = snprintf(NULL, 0, "Reply of ") + size;
				if (sizeof(bHDLC_txbuf) - txbuf_in >= n + 1) {
					sprintf(&bHDLC_txbuf[txbuf_in], "Reply of %.*s", size, data);
					if (bHDLC_send(link, &bHDLC_txbuf[txbuf_in], n) == n) {
						txbuf_cnt += n;
						txbuf_in += n;
					}
				} else {
					txbuf_blocked = 1;
				}
			}
			return 0;
		}
		case HDLC_EVENT_SENT:
			txbuf_cnt -= size;
			if (txbuf_cnt == 0) {
				txbuf_blocked = 0;
				txbuf_in = 0;
			}
			return 0;
	}
	return 0;
}

void
server_resp_main(bHDLC_link *link)
{
	bHDLC_set_rxbuf(link, bHDLC_rxbuf, sizeof(bHDLC_rxbuf));
	bHDLC_set_up(link);

	connected = 0;

	while(1) {
		bHDLC_poll();
	}
}
