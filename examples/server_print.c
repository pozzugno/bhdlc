#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include "bHDLC/bHDLC.h"
#include "timer.h"

static unsigned char bHDLC_rxbuf[60 + BHDLC_FRAME_OVERHEAD];

static unsigned char connected = 0;

size_t
server_print_cb(bHDLC_link *link, bHDLC_Event event, const void *data, size_t size)
{
	switch(event) {
		case HDLC_EVENT_CONNECTED:
			connected = 1;
			return 0;
		case HDLC_EVENT_DISCONNECTED:
			connected = 0;
			return 0;
		case HDLC_EVENT_RECV: {
			const unsigned char *d = data;
			while(size--) {
				printf("%c", *d); fflush(stdout);
				d++;
			}
			return 0;
		}
		case HDLC_EVENT_SENT:
			return 0;
	}
	return 0;
}

void
server_print_main(bHDLC_link *link, int argc, char *argv)
{
	bHDLC_set_rxbuf(link, bHDLC_rxbuf, sizeof(bHDLC_rxbuf));
	bHDLC_set_up(link);

	connected = 0;

	while(1) {
		bHDLC_poll();
	}
}

