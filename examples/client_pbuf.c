#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <stdint.h>
#include "bHDLC/bHDLC.h"
#include "timer.h"

#if BHDLC_TXFIFO

static unsigned char bHDLC_rxbuf[63];		// 3 byte in più del payload necessario

static unsigned char connected = 0;
static Timer tmr_connection;

#define MAX_MSG			5
#define MSG_LEN			17					// strlen("Message number x\n")
static const char msg_fmt[] = "Message number %d\n";
static unsigned char txbuf[MAX_MSG * MSG_LEN];
static struct pbuf pbuf[MAX_MSG];

size_t
client_pbuf_cb(bHDLC_link *link, bHDLC_Event event, void *data, size_t size)
{
	switch(event) {
		case HDLC_EVENT_CONNECTED:
			printf("Connected\n");
			connected = 1;
			return 0;
		case HDLC_EVENT_DISCONNECTED:
			printf("Disconnected\n");
			connected = 0;
			TimerSet(&tmr_connection, 0);		// Try a new connetion immediately
			return 0;
		case HDLC_EVENT_RECV:
			return 0;
		case HDLC_EVENT_SENT:
			return 0;
	}
	return 0;
}

void
client_pbuf_loop(bHDLC_link *link)
{
	bHDLC_set_rxbuf(link, bHDLC_rxbuf, sizeof(bHDLC_rxbuf));
	bHDLC_set_txbuf(link, txbuf, sizeof(txbuf), pbuf, sizeof(pbuf) / sizeof(pbuf[0]));
	bHDLC_set_up(link);

	connected = 0;
	TimerSet(&tmr_connection, 0);

	while(1) {
		if (!connected && TimerExpired(&tmr_connection)) {
			TimerSet(&tmr_connection, 5000);
			bHDLC_close(link);
			bHDLC_connect(link);
		}
		bHDLC_poll();
		if (_kbhit() && connected) {
			char c = _getch();
			if ((c >= '1') && (c <= '9')) {
				for (unsigned int i = 1; i <= c - '0'; i++) {
					char msg[MSG_LEN + 1];
					sprintf(msg, msg_fmt, i);
					if (bHDLC_write(link, msg, strlen(msg)) == strlen(msg)) {
						bHDLC_send(link);
					}
				}
			}
		}
	}
}

#endif
