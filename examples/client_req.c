#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <stdint.h>
#include "bHDLC/bHDLC.h"
#include "timer.h"

static unsigned char bHDLC_rxbuf[60 + BHDLC_FRAME_OVERHEAD];
static char bHDLC_txbuf[256];
static size_t txbuf_cnt;
static unsigned int req_idx = 0;

static unsigned char connected = 0;
static Timer tmr_connection;

size_t
client_req_cb(bHDLC_link *link, bHDLC_Event event, void *data, size_t size)
{
	switch(event) {
		case HDLC_EVENT_CONNECTED:
			printf("Connected\n");
			connected = 1;
			return 0;
		case HDLC_EVENT_DISCONNECTED:
			printf("Disconnected\n");
			connected = 0;
			TimerSet(&tmr_connection, 0);		// Try a new connetion immediately
			return 0;
		case HDLC_EVENT_RECV:
			printf("<- %.*s\n", size, (const char *)data);
			break;
		case HDLC_EVENT_SENT:
			break;
	}
	return 0;
}

void
client_req_main(bHDLC_link *link)
{
	bHDLC_set_rxbuf(link, bHDLC_rxbuf, sizeof(bHDLC_rxbuf));
	bHDLC_set_up(link);

	connected = 0;
	TimerSet(&tmr_connection, 0);

	while(1) {
		if (!connected && TimerExpired(&tmr_connection)) {
			TimerSet(&tmr_connection, 5000);
			bHDLC_close(link);
			bHDLC_connect(link);
		}
		bHDLC_poll();
		if (_kbhit() && connected) {
			char c = _getch();
			printf("\n");
			if ((c >= '1') && (c <= '9')) {
				txbuf_cnt = 0;
				for (int i = 0; i <= c - '1'; i++) {
					int n = snprintf(NULL, 0, "Request %3d", req_idx);
					if (n < sizeof(bHDLC_txbuf) - txbuf_cnt - 1) {
						n = sprintf(&bHDLC_txbuf[txbuf_cnt], "Request %3d", req_idx);
						bHDLC_send(link, &bHDLC_txbuf[txbuf_cnt], n);
						printf("-> %.*s\n", n, &bHDLC_txbuf[txbuf_cnt]);
						txbuf_cnt += n;
						req_idx++;
					}
				}
			}
		}
	}
}
