#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include "bHDLC/bHDLC.h"
#include "timer.h"

static unsigned char bHDLC_rxbuf[60 + BHDLC_FRAME_OVERHEAD];
static unsigned char bHDLC_txbuf[50];

static unsigned char connected = 0;

static int finished = 0;
static Timer tmr_connection;
static FILE *f;

static void
trysend(bHDLC_link *link)
{
	int c;
	int cnt = 0;
	while((cnt < sizeof(bHDLC_txbuf)) && (c = fgetc(f)) != EOF) {
		bHDLC_txbuf[cnt++] = (char)c;
	}
	if (cnt == 0) {
		finished = 1;
		printf("Finished\n");
	} else {
		bHDLC_send(link, bHDLC_txbuf, cnt);
	}
}

size_t
client_sendfile_cb(bHDLC_link *link, bHDLC_Event event, const void *data, size_t size)
{
	switch(event) {
		case HDLC_EVENT_CONNECTED:
			connected = 1;
			finished = 0;
			trysend(link);
			return 0;
		case HDLC_EVENT_DISCONNECTED:
			connected = 0;
			if (f != NULL) {
				fprintf(stderr, "Disconnection during transmission\n");
				fclose(f);
			}
			finished = 1;
			return 0;
		case HDLC_EVENT_RECV: {
			const unsigned char *d = data;
			while(size--) {
				printf("%c", *d++); fflush(stdout);
			}
			return 0;
		}
		case HDLC_EVENT_SENT:
			if (f != NULL) {
				trysend(link);
				if (finished) {
					fclose(f);
					f = NULL;
				}
			}
			break;
	}
	return 0;
}

void
client_sendfile_main(bHDLC_link *link, int argc, char *argv[])
{
	bHDLC_set_rxbuf(link, bHDLC_rxbuf, sizeof(bHDLC_rxbuf));
	bHDLC_set_up(link);

	if (argc == 0) {
		fprintf(stderr, "Specify at least one file at the command line\n");
		exit(EXIT_FAILURE);
	}
	f = fopen(*argv, "r");
	if (f == NULL) {
		fprintf(stderr, "Error opening file %s\n", *argv);
		exit(EXIT_FAILURE);
	}

	connected = 0;
	TimerSet(&tmr_connection, 0);

	while(1) {
		if (!connected && TimerExpired(&tmr_connection)) {
			TimerSet(&tmr_connection, 5000);
			bHDLC_close(link);
			bHDLC_connect(link);
		}
		bHDLC_poll();
		if (finished && !connected) {
			break;		// Exit
		}
	}
}
