/* bHDLC_fcs.c */
#include <stdint.h>
#include "bHDLC/bHDLC_fcs.h"

/**
 * Calculate the next value of the checksum.
 *
 * @param crc, old value of checksum
 * @param data, next byte
 * @return value of next checksum
 */
static uint8_t
_crc_ibutton_update(uint8_t crc, uint8_t data)
{
	uint8_t i;

	crc = crc ^ data;
	for (i = 0; i < 8; i++) {
		if (crc & 0x01)
			crc = (crc >> 1) ^ BHDLC_FCS_SEEDVALUE;
		else
			crc >>= 1;
	}
	return crc;
}

/**
 * Calculate the checksum of a string of bytes.
 *
 * @param old_crc, starting value
 * @param data, pointer to data
 * @param size, length of data string
 * @return value of checksum
 */
unsigned char
bHDLC_fcs(unsigned char old_crc, const void *data, unsigned char size)
{
	const unsigned char *d = data;
	while(size--) {
		old_crc = _crc_ibutton_update(old_crc, *d++);
	}
	return old_crc;
}
