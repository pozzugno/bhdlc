/* bHDLC.c */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include "bHDLC/bHDLC.h"
#include "bHDLC/sio.h"
#include "bHDLC/bHDLC_fcs.h"
#include "bHDLC/bHDLC_frame.h"
#include "qassert.h"

Q_DEFINE_THIS_MODULE("bHDLC")

/* Main struct associated to a bHDLC link (the structure is hidden to the application). */
struct bHDLC_link {
	unsigned char		allocated;		// We don't have and don't want to use dynamic memory
	unsigned char		up;				// When the interface is fully configured (ready for connection)
	sio_fd_t 			sio_fd;			// Serial I/O device passed to sio functions (customizable in sio.h)
	unsigned char 		addr;			// TODO: my address (not used)

	unsigned char 		tx_seq;			// Sequence number of last I-frame already send
	unsigned char 		rx_seq;			// Sequence number of last I-frame received

	bHDLC_cb			link_cb;		// User callback for events
	const void 			*arg;			// TODO: Generic pointer completely managed by the application (not used)

	enum {
		LINK_DISCONNECTED = -1,			// Disconnected
		LINK_SABM_SEND,					// SABM U-Frame already send, waiting for the answer from the peer
		LINK_CONNECTED,					// Connected (UA U-Frame received from the peer)
	} state;							// Internal state of the link (valid if the link is up)

	unsigned char		peer_MTU;		// Maximum Transmission Unit of the peer (maximum length in bytes)

	unsigned char		frame_esc;		// HDLC framing uses an escape character
	unsigned char 		*rxbuf;			// Buffer for incoming frames
	size_t 				rxbuf_size;		// Length of rxbuf
	size_t 				rxcnt;			// Length of the current received frame

	/* Queue of output frames. Here resides frames already transmitted but not acked yet, and
	 * frame not transmitted yet (because the "sliding window" is full). */
	struct {
		unsigned char	retr_cnt;		// Available retransmission
		sys_ticks_t 	ack_ticks;		// ACK timeout (if it expires, the frame is retransmitted)
		unsigned char 	tx_seq;			// N(S) sequence number
		size_t 			size;			// Dimension of payload data
		const void 		*data;			// Payload data
	} txqueue[BHDLC_TXQUEUE_SIZE];
	unsigned char txq_in, txq_out, txq_cnt;		// Variables used to manage txqueue[] array as a FIFO
	unsigned char num_frames_waiting_ack;		// <= BHDLC_TXWINDOW_SIZE <= txq_cnt <= BHDLC_TXQUEUE_SIZE
	unsigned char send_ack;				// It's just a flag to avoid unnecessary S-frames (ack)

	sys_ticks_t			wait_ack_timeout;	// Amount of time to wait for the ack, before retransmitting (or giving up)
	unsigned char		retr_max;		// Maximum number of retransmissions (before forced disconnection)
#if BHDLC_STATISTICS
	unsigned long		errors;			// Number of corrupted frames
	unsigned long		total_retr;		// Total bumber of retransmissions
#endif
};

/* Statically allocated links. I decided to use this strategy for two reasons:
 *
 *     1.   returning a link pointer from bHDLC_add() let me hide
 *          structure definition
 *     2.   application can call bHDLC_poll() only one time and not
 *          for each link
 *     3.   usually number of links are known and fixed at compile time
 */
static bHDLC_link links[BHDLC_LINKS_NUM];

/* HDLC frame starts and ends with a Flag byte. Internal bytes identical to Flag
 * or Escape characters must be escaped. */
#define HDLC_FRAME_FLAG			0x7E		// Start and end Flag
#define HDLC_FRAME_ESC			0x7D		// Escape character
#define HDLC_FRAME_ESCAPED(c)	(c ^ 0x20)	// How to escape a character

#if BHDLC_DEBUG
void
print_frame(const char *suffix, HDLC_Frame *frm)
{
	BHDLC_DEBUG_PRINTF((suffix));
	if (frm->type == I_FRAME) {
		BHDLC_DEBUG_PRINTF(("I %02X (R%d)%c(S%d) ",
			 frm->addr, frm->rx_seq, frm->pf_flag ? 'P' : ' ', frm->tx_seq));
	} else if (frm->type == S_FRAME) {
		BHDLC_DEBUG_PRINTF(("S %02X (R%d)%c(S ) ",
			 frm->addr, frm->rx_seq, frm->pf_flag ? 'P' : ' '));
	} else {
		BHDLC_DEBUG_PRINTF(("U %02X (%02X)%c ",
			 frm->addr, frm->type, frm->pf_flag ? 'P' : ' '));
	}

	const unsigned char *d = frm->data;
	size_t s = frm->size;
	while(s--) {
//		BHDLC_DEBUG_PRINTF(("%c", ((*d == '\n') || (*d == ' ')) ? '_' : *d));
		BHDLC_DEBUG_PRINTF(("%02X", *d));
		d++;
	}
	BHDLC_DEBUG_PRINTF(("(%u) ", frm->size));
	BHDLC_DEBUG_PRINTF(("\n"));
}
#endif

static inline unsigned int
getuint16_be(const void *data)
{
	const unsigned char *p = data;
	unsigned int ux;
	ux =           (p[0] & 0xFF);
	ux = ux << 8 | (p[1] & 0xFF);
	return ux;
}

static inline void
putuint16_be(void *data, unsigned int x)
{
	unsigned char *p = data;
	p[0] = (x >> 8) & 0xFF;
	p[1] = x & 0xFF;
}

/**
 * Check if a timer has expired.
 *
 * @param tmr, the timer to check
 * @return 1, if the timer has expired
 */
static int
timer_expired(sys_ticks_t tmr)
{
	sys_ticks_t now = sys_now();
	sys_ticks_t diff = sys_now() - tmr;
	Q_ASSERT((sizeof(now) == 1) || (sizeof(now) == 2) || (sizeof(now) == 4));

	if (sizeof(now) == 1) {
		if (diff < 0x80) return 1;
	} else if (sizeof(now) == 2) {
		if (diff < 0x8000) return 1;
	} else if (sizeof(now) == 4) {
		if (diff < 0x80000000) return 1;
	}
	return 0;
}

/**
 * Actually send a single byte to the serial port.
 *
 * @param link, the bHDLC link
 * @param c, the character to send to the serial port
 */
static void
send_byte(bHDLC_link *link, unsigned char c)
{
	if ((c == HDLC_FRAME_FLAG) || (c == HDLC_FRAME_ESC)) {
		sio_putc(HDLC_FRAME_ESC, link->sio_fd);
		sio_putc(HDLC_FRAME_ESCAPED(c), link->sio_fd);
	} else {
		sio_putc(c, link->sio_fd);
	}
}

/**
 * Send now a complete frame to the serial port.
 *
 * @param link, the bHDLC link
 * @param frm, the frame to transmit
 */
static void
send_frame(bHDLC_link *link, HDLC_Frame *frm)
{
	const unsigned char *d = frm->data;
	size_t size = frm->size;
	unsigned char ctrl = 0x00;
	unsigned char fcs = BHDLC_FCS_SEEDVALUE;

#if BHDLC_DEBUG
	print_frame("-> ", frm);
#endif
	/* Start flag */
	sio_putc(HDLC_FRAME_FLAG, link->sio_fd);
	/* Address */
	send_byte(link, frm->addr);
	fcs = bHDLC_fcs(fcs, &frm->addr, 1);
	/* Control byte */
	if(frm->type == I_FRAME) {
		ctrl = (frm->rx_seq << 5) | (frm->tx_seq << 1);
	} else if(frm->type == S_FRAME) {
		ctrl = (frm->rx_seq << 5) | 0x01;
	} else {
		ctrl = frm->type | (0x10 * frm->pf_flag);
	}
	if (frm->pf_flag) ctrl |= 0x10;
	send_byte(link, ctrl);
	fcs = bHDLC_fcs(fcs, &ctrl, 1);
	/* Data */
	if (frm->type != S_FRAME) {
		while(size--) {
			send_byte(link, *d);
			fcs = bHDLC_fcs(fcs, d, 1);
			d++;
		}
	}
	/* FCS */
	send_byte(link, fcs);
	/* Final Flag */
	sio_putc(HDLC_FRAME_FLAG, link->sio_fd);
}

/**
 * Sends an I-frame. Payload is taken from link->ofrm.
 *
 * @param link, bHDLC link
 */
static void
send_I_frame(bHDLC_link *link, unsigned char seq, const void *data, size_t size)
{
	HDLC_Frame frm;
	frm.type = I_FRAME;
	frm.addr = link->addr;
	frm.tx_seq = seq;
	frm.rx_seq = HDLC_SEQ(link->rx_seq + 1);
	frm.data = data;
	frm.size = size;
	frm.pf_flag = 1;
	send_frame(link, &frm);

	link->send_ack = 0;
}

/**
 * Sends an S-frame.
 *
 * @param link, bHDLC link
 */
static void
send_S_frame(bHDLC_link *link)
{
	HDLC_Frame frm;

	frm.type = S_FRAME;
	frm.addr = link->addr;
	frm.rx_seq = HDLC_SEQ(link->rx_seq + 1);
	frm.pf_flag = 1;
	frm.size = 0;
	send_frame(link, &frm);
}

/**
 * Sends an U-frame.
 *
 * @param link, bHDLC link
 * @param utype, type of U-Frame (SABM, UA)
 * @param data, payload
 * @param size, size of payload
 */
static void
send_U_frame(bHDLC_link *link, FrameType utype, const void *data, size_t size)
{
	HDLC_Frame frm;

	frm.type = utype;
	frm.addr = link->addr;
	frm.pf_flag = 1;
	frm.data = (void *)data;
	frm.size = size;
	send_frame(link, &frm);
}

/**
 * Check if a complete frame is received.
 *
 * @param link, bHDLC link
 * @param frm, pointer to the frame to be filled
 * @return -  0 if frame isn't available
 *         -  1 if a frame is available (frm is filled)
 *         - -1 if error occurred (FCS or frame too short)
 */
static int
recv_frame(bHDLC_link *link, HDLC_Frame *frm)
{
	int c;

	while((c = sio_getc(link->sio_fd)) >= 0) {
		if (c == HDLC_FRAME_FLAG) {
			if (link->rxcnt) {			// A complete frame is received
				if ((link->rxcnt < 3) || bHDLC_fcs(BHDLC_FCS_SEEDVALUE, link->rxbuf, link->rxcnt)) {
					/* Frame too short or corruption, silently discard frame and returns -1 */
					link->rxcnt = 0;
#if BHDLC_STATISTICS
					link->errors++;
#endif
					return -1;
				} else {
					/* A complete frame is received */
					frm->addr = link->rxbuf[0];
					frm->rx_seq = link->rxbuf[1] >> 5;
					frm->pf_flag = link->rxbuf[1] & 0x10;
					if ((link->rxbuf[1] & 0x03) == 0x01) {
						frm->type = S_FRAME;
						frm->size = 0;
					} else if ((link->rxbuf[1] & 0x03) == 0x03) {
						frm->type = link->rxbuf[1] & ~0x10;
						frm->data = &link->rxbuf[2];
						frm->size = link->rxcnt - 3;
					} else if ((link->rxbuf[1] & 0x01) == 0x00) {
						frm->type = I_FRAME;
						frm->tx_seq = (link->rxbuf[1] >> 1) & 0x07;
						frm->data = &link->rxbuf[2];
						frm->size = link->rxcnt - 3;		// Address, Control and FCS
					}
#if BHDLC_DEBUG
					print_frame("<- ", frm);
#endif
					link->rxcnt = 0;
					return 1;
				}
			}
		} else if (c == HDLC_FRAME_ESC) {
			link->frame_esc = 1;
		} else if (link->rxcnt < link->rxbuf_size) {
			if (link->frame_esc) {
				c = HDLC_FRAME_ESCAPED(c);
				link->frame_esc = 0;
			}
			link->rxbuf[link->rxcnt++] = c;
		}
	}

	return 0;
}

/**
 * The link has just disconnected (it was connected).
 */
static void
just_disconnected(bHDLC_link *link)
{
	link->link_cb(link, HDLC_EVENT_DISCONNECTED, NULL, 0);
	link->state = LINK_DISCONNECTED;
#if BHDLC_DEBUG
	BHDLC_DEBUG_PRINTF(( "bHDLC: Disconnected\n"));
#endif
}

/**
 * The link has just connected (it was disconnected).
 */
static void
just_connected(bHDLC_link *link, unsigned int peer_MTU)
{
	link->peer_MTU = peer_MTU;
	link->state = LINK_CONNECTED;
	link->rx_seq = link->tx_seq = 0;
	link->txq_in = link->txq_out = link->txq_cnt = 0;
	link->num_frames_waiting_ack = 0;
#if BHDLC_DEBUG
	BHDLC_DEBUG_PRINTF(("bHDLC: Connected (peer MTU is %d)\n", link->peer_MTU));
#endif
	link->link_cb(link, HDLC_EVENT_CONNECTED, NULL, 0);
}

/**
 * Process a received frame.
 */
static void
process_frame(bHDLC_link *link, HDLC_Frame *rxfrm)
{
	/* Whatever is the current link state, why don't accept a new connection request? */
	if (rxfrm->type == U_FRAME_SABM) {
		/* TODO: call callback to let the application accept the request.
		 * At the moment, we accept automatically the request. */
		if (link->state == LINK_CONNECTED) {
			just_disconnected(link);
		}
		unsigned char data[2];
		putuint16_be(data, link->rxbuf_size);			// Sends our MTU to prevent big frames
		send_U_frame(link, U_FRAME_UA, data, sizeof(2));
		/* Perform the connection */
		unsigned int peer_MTU = 0;
		if (rxfrm->size >= 2) {				// Peer sends its MTU
			peer_MTU = getuint16_be(rxfrm->data);
		}
		just_connected(link, peer_MTU);
		return;		// Frame already processed
	}

	switch(link->state) {
		case LINK_DISCONNECTED:				// Connection requests already processed
			break;

		case LINK_CONNECTED: {
			unsigned char duplicated = 0;

			/* Here only I- and S-frames are supported */
			if ((rxfrm->type != I_FRAME) && (rxfrm->type != S_FRAME)) {
				break;
			}

			/* First of all, check if I-frame matches what we are expecting.
			 * If it's ok, update our rx sequence number so we automatically ack
			 * with next output I-frame, if any. */
			link->send_ack = 0;
			if (rxfrm->type == I_FRAME) {
				if (rxfrm->tx_seq == HDLC_SEQ(link->rx_seq + 1)) {		// Expected I-frame received
					link->rx_seq = rxfrm->tx_seq;			// Update now rx sequence number
															// so we automatically ACK on next I-frame
					link->send_ack = 1;
				} else {									// Duplicated frame
					duplicated = 1;
					link->send_ack = 1;		// Duplicated means the node needs an ack again
											// (maybe the previous was corrupted)
				}
			}

			/* If we are waiting for acks, check if some has arrived just now */
			while(link->num_frames_waiting_ack > 0) {
				/* Remember, rxfrm->rx_seq means the *next* frame to receive.
				 * The rx frame acks if its sequence number is in the closed interval [A-B]. */
				unsigned char A = HDLC_SEQ(link->txqueue[link->txq_out].tx_seq + 1);
				unsigned char B = HDLC_SEQ(A + link->num_frames_waiting_ack);
				/* The interval match must be made with modulo N arithmetic */
				if (HDLC_SEQ(rxfrm->rx_seq - A) < HDLC_SEQ(B - A)) {
					/* Frame link->txqueue[link->txq_out] acked. Inform application. */
					link->link_cb(link, HDLC_EVENT_SENT,
									link->txqueue[link->txq_out].data,
									link->txqueue[link->txq_out].size);
					/* Remove the acked frame from the output queue */
					link->txq_out = (link->txq_out + 1) % BHDLC_TXQUEUE_SIZE;
					Q_ASSERT(link->txq_cnt > 0);
					link->txq_cnt--;
					link->num_frames_waiting_ack--;
				} else {
					break;		// Stop the check, next frames aren't acked for sure
				}
			}

			/* Maybe now we can transmit addional pending frames */
			Q_ASSERT(link->num_frames_waiting_ack <= link->txq_cnt);
			while ((link->txq_cnt - link->num_frames_waiting_ack > 0) &&
							(link->num_frames_waiting_ack < BHDLC_TXWINDOW_SIZE)) {
				size_t txqi = (link->txq_out + link->num_frames_waiting_ack) % BHDLC_TXQUEUE_SIZE;
				send_I_frame(link, link->txqueue[txqi].tx_seq, link->txqueue[txqi].data, link->txqueue[txqi].size);
				link->txqueue[txqi].ack_ticks = sys_now() + link->wait_ack_timeout;
				link->txqueue[txqi].retr_cnt = link->retr_max;
				link->num_frames_waiting_ack++;
			}

			/* Pass received data to the application */
			if ((rxfrm->type == I_FRAME) && !duplicated) {
				link->link_cb(link, HDLC_EVENT_RECV,
								rxfrm->data, rxfrm->size);
			}

			/* Send S-frame (ACK), if necessary */
			if (link->send_ack) {
				send_S_frame(link);
			}

			break;
		}

		case LINK_SABM_SEND:                        // Accepts only UA-frames (silently discard others)
			if (rxfrm->type == U_FRAME_UA) {
				/* Perform the connection */
				unsigned int peer_MTU = 0;
				if (rxfrm->size >= 2) {
					peer_MTU = getuint16_be(rxfrm->data);
				}
				just_connected(link, peer_MTU);
			}
			break;
	}
}

/**
 * Adds and allocate a new link.
 *
 * @param addr, address of the link
 * @param fd, serial device descriptor
 * @param cb, application callback function
 * @return - NULL, if out of memory
 *         - pointer to the newly allocated link
 */
bHDLC_link *
bHDLC_add(unsigned char addr, sio_fd_t fd, bHDLC_cb link_cb)
{
	Q_ASSERT(link_cb != NULL);
	int link_idx = -1;
	for (unsigned int i = 0; i < sizeof(links); i++) {
		if (!links[i].allocated) {
			link_idx = i;
			break;
		}
	}
	if (link_idx < 0) return NULL;

    bHDLC_link *link = &links[link_idx];
    link->allocated = 1;
    link->addr = addr;
    link->sio_fd = fd;
    link->link_cb = link_cb;

	link->up = 0;
    link->rxbuf = NULL;
    link->rxbuf_size = 0;

    link->retr_max = BHDLC_RETRY_MAX;
    link->wait_ack_timeout = BHDLC_WAITACK_TIMEOUT;

    return link;
}

/**
 * Sets a receive buffer, allocated by the application.
 * Without a receive buffer, the link can't be activated.
 * The bytes received over the dimension of this buffer will be silently discarded.
 * Most probably, the entire frame will be discarded for corruption.
 *
 * @param link, bHDLC link returned by bHDLC_add()
 * @param rxbuf, pointer to the buffer
 * @param rxbuf_size, size of the receive buffer.
 * @return - 0
 */
int
bHDLC_set_rxbuf(bHDLC_link *link, void *rxbuf, size_t rxbuf_size)
{
	Q_ASSERT(link != NULL);
	Q_ASSERT(link->allocated);
	Q_ASSERT(!link->up);
	Q_ASSERT((rxbuf != NULL) && (rxbuf_size != 0));

	link->rxbuf = rxbuf;
	link->rxbuf_size = rxbuf_size;
	return 0;
}

/**
 * Sets the maximum number of retransmission for a BHDLC link.
 * The link mustn't be already activated.
 * If this function isn't called, the default value BHDLC_RETRY_MAX
 * will be used.
 *
 * @param link, bHDLC link returned by bHDLC_add()
 * @param max_retr, new maximum number of retransmission
 */
void
bHDLC_set_maxretr(bHDLC_link *link, unsigned char max_retr)
{
	Q_ASSERT(link != NULL);
	Q_ASSERT(link->allocated);
	Q_ASSERT(!link->up);
	link->retr_max = max_retr;
}

/**
 * Sets the ACK timeout for a link. The link mustn't be activated.
 * The timeout interval is defined as number of ticks, the time unit
 * returned by sys_now().
 *
 * @param link, bHDLC link returned by bHDLC_add()
 * @param interval, timeout interval in system ticks
 */
void
bHDLC_set_acktout(bHDLC_link *link, sys_ticks_t interval)
{
	Q_ASSERT(link != NULL);
	Q_ASSERT(link->allocated);
	Q_ASSERT(!link->up);
	link->wait_ack_timeout = interval;
}

/**
 * Activates a bHDLC link.
 * When a link is activated, it is ready to receive connection requests (and
 * automatically accepts them).
 *
 * @param link, bHDLC link
 * @return - 0, if success
 *         - -1, if error occurred (serial init function returns an error)
 */
int
bHDLC_set_up(bHDLC_link *link)
{
	Q_ASSERT(link != NULL);
	Q_ASSERT((link->rxbuf != NULL) && (link->rxbuf_size != 0));
	if (sio_init(link->sio_fd) < 0) return -1;
	link->up = 1;
	link->state = LINK_DISCONNECTED;

	link->rxcnt = 0;

#if BHDLC_STATISTICS
	/* Reset errors for statistics */
	link->errors = 0;
	link->total_retr = 0;
#endif

	return 0;
}

/**
 * Sends a connection request to the peer.
 * Our MTU is also sends to the peer to prevent big frames.
 * The request can be send only if the link was previously activated with
 * bHDLC_set_up() and disconnected.
 *
 * @param link, bHDLC link
 * @return - 0
 */
int
bHDLC_connect(bHDLC_link *link)
{
	Q_ASSERT((link != NULL) && link->up);
	Q_ASSERT(link->state == LINK_DISCONNECTED);

	unsigned char data[2];
	putuint16_be(data, link->rxbuf_size - 3);		// Address, Control and FCS are saved in link->rxbuf
    send_U_frame(link, U_FRAME_SABM, data, sizeof(data));
	/* After sending a connection request, the link goes in SABM_SEND state. */
    link->state = LINK_SABM_SEND;
	return 0;
}

/**
 * Abruptly switch to disconnected state, without sending disconnection request
 * to the peer.
 *
 * @param link, bHDLC link
 * @return - 0
 */
int
bHDLC_close(bHDLC_link *link)
{
	Q_ASSERT((link != NULL) && link->up !=0);
	switch(link->state) {
		case LINK_DISCONNECTED:		// Nothing to do
			break;
		case LINK_SABM_SEND:		// Nothing to do
			break;
		case LINK_CONNECTED:		// TODO: We should send DISC U-frame
			break;
	}
	link->state = LINK_DISCONNECTED;
	return 0;
}

/**
 * This function must be called continuously by the application. It does three things:
 *   - check and process incoming frames
 *   - transmits pending frames in FIFO buffer (if it was enabled)
 *   - transmits ack of the last received frame, if needed
 */
void
bHDLC_poll(void)
{
	for (size_t i = 0; i < sizeof(links) / sizeof(links[0]); i++) {
		/* Loop through every allocated and activated links. */
		bHDLC_link *link = &links[i];
		if (!link->allocated || !link->up)
			continue;

		/* Check and process incoming frames */
		int ret;
		HDLC_Frame rxfrm;
		while((ret = recv_frame(link, &rxfrm)) > 0) {
			process_frame(link, &rxfrm);
		}

		/* For connected links only, check for timeouts */
		if (link->state == LINK_CONNECTED) {
			/* Check ack timeout */
			if (link->num_frames_waiting_ack && timer_expired(link->txqueue[link->txq_out].ack_ticks)) {
				if (link->txqueue[link->txq_out].retr_cnt--) {
					for (unsigned char i = 0; i < link->num_frames_waiting_ack; i++) {
						unsigned char tq_idx = (link->txq_out + i) % BHDLC_TXQUEUE_SIZE;
#if BHDLC_DEBUG
						BHDLC_DEBUG_PRINTF(("Retr: "));
#endif
						send_I_frame(link, link->txqueue[tq_idx].tx_seq, link->txqueue[tq_idx].data, link->txqueue[tq_idx].size);
						link->txqueue[tq_idx].ack_ticks = sys_now() + link->wait_ack_timeout;
#if BHDLC_STATISTICS
						link->total_retr++;			// Update statistics of retransmissions
#endif
					}
				} else {
					just_disconnected(link);	// Force a disconnection
				}
			}
		}
	}
}

/**
 *
 * @param link, bHDLC link
 * @param data, pointer to the data
 * @param size, length of data
 * @return size, if the frame was correctly enqueued
 *         0, if the queue is full
 */
int
bHDLC_send(bHDLC_link *link, const void *data, size_t size)
{
	Q_ASSERT((link != NULL) && link->up);
	Q_ASSERT(link->state == LINK_CONNECTED);

	if (link->txq_cnt < BHDLC_TXQUEUE_SIZE) {
		unsigned char i = link->txq_in;
		link->tx_seq = HDLC_SEQ(link->tx_seq + 1);		// New tx sequence number
		link->txqueue[i].tx_seq = link->tx_seq;
		link->txqueue[i].data = data;
		link->txqueue[i].size = size;
		// Move FIFO indexes
		link->txq_in = (link->txq_in + 1) % BHDLC_TXQUEUE_SIZE;
		link->txq_cnt++;
		// Really send the frame if the tx window isn't full
		if (link->num_frames_waiting_ack < BHDLC_TXWINDOW_SIZE) {
			send_I_frame(link, link->txqueue[i].tx_seq, link->txqueue[i].data, link->txqueue[i].size);
			link->txqueue[i].ack_ticks = sys_now() + link->wait_ack_timeout;
			link->txqueue[i].retr_cnt = link->retr_max;
			Q_ASSERT(link->num_frames_waiting_ack < link->txq_cnt);
			link->num_frames_waiting_ack++;
		}
		return size;
	}
	return 0;
}
