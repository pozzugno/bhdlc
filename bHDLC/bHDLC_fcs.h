#ifndef BHDLC_FCS_H
#define BHDLC_FCS_H

#define BHDLC_FCS_SEEDVALUE			0x8C

unsigned char bHDLC_fcs(unsigned char old_crc, const void *data, unsigned char size);

#endif
