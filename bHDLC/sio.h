#ifndef SIO_H
#define SIO_H

typedef void *sio_fd_t;
int sio_init(sio_fd_t fd);
int sio_putc(int c, sio_fd_t fd);
int sio_getc(sio_fd_t fd);

#endif
