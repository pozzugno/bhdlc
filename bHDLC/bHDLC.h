#ifndef bHDLC_H
#define bHDLC_H

#define bHDLC_VERSION      "001a"

#include <stddef.h>
#include "bHDLC/sio.h"
#include "bHDLC_conf.h"

#define BHDLC_FRAME_OVERHEAD			3

/* bHDLC manages only one timer for retransmission. The time interval is defined as multiple
 * of system ticks, as returned by sys_now() function. */
typedef BHDLC_SYS_TICKS_T sys_ticks_t;

/* Data structure of a bHDLC link (the details are hidden to the user) */
typedef struct bHDLC_link bHDLC_link;

/* bHDLC implementation calls a user callback when certain events occur */
typedef enum {
	HDLC_EVENT_CONNECTED,			/* The link is now connected */
	HDLC_EVENT_DISCONNECTED,		/* The link is now disconnected */
	HDLC_EVENT_RECV,				/* Some data is received */
	HDLC_EVENT_SENT,				/* Some data is received */
} bHDLC_Event;
/* The user callback that is called by bHDLC */
typedef size_t (*bHDLC_cb)(bHDLC_link *link, bHDLC_Event event, const void *data, size_t size);

bHDLC_link *bHDLC_add(unsigned char addr, sio_fd_t fd, bHDLC_cb link_cb);
int bHDLC_set_rxbuf(bHDLC_link *link, void *rxbuf, size_t rxbuf_size);
void bHDLC_set_maxretr(bHDLC_link *link, unsigned char max_retr);
void bHDLC_set_acktout(bHDLC_link *link, sys_ticks_t interval);
int bHDLC_set_up(bHDLC_link *link);
int bHDLC_connect(bHDLC_link *link);
int bHDLC_close(bHDLC_link *link);
void bHDLC_poll(void);
int bHDLC_send(bHDLC_link *link, const void *data, size_t size);

sys_ticks_t sys_now(void);

#endif
