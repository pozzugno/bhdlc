#ifndef BHDLC_FRAME_H
#define BHDLC_FRAME_H

#include <stddef.h>
#include "bHDLC/bHDLC.h"

/* Error recovery mechanism of bHDLC protocol is identical to standard HDLC.
 * The sender marks every transmitted frame with a sequence number N(S). This
 * value is encoded and send inside the frame.
 * By analizing N(S) the receiver can understand if the frame is duplicated or
 * a new one. If it is a new one, the receiver sends an ACK to the sender.
 * The ACK is another sequence number, N(R) that is the new sequence number
 * that it wants to receive. N(R) is transmitted in I- and S-frames.
 * The sequence number is a counter from zero (immediately after the connection)
 * to the maximum value. After that, the value wraps-around to zero again. */
#define HDLC_SEQ(x)						( (unsigned)(x) % 8 )

typedef enum {
	I_FRAME,
	S_FRAME,
	U_FRAME_SABM = 0x2F,	// Value identical to control bytes with P/F flag zeroed
	U_FRAME_UA = 0x63		// Value identical to control bytes with P/F flag zeroed
} FrameType;

/* One received or transmitted frame structure. */
typedef struct Frame {
	unsigned char addr;			// Address
	unsigned char pf_flag;		// P/F flag
	FrameType type;				// Type of frame
	unsigned char rx_seq;		// N(R), valid only for I- and S-frames
	unsigned char tx_seq;		// N(S), valid only for I-frames
	const unsigned char *data;	// Frame payload, valid only for I- and U-frames
	size_t size;				// Length of data member
} HDLC_Frame;

#endif
