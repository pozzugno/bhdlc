#ifndef BHDLC_CONF_H
#define BHDLC_CONF_H

#include "bHDLC_conf_user.h"		// User-defined values

/* bHDLC can generate some messages for debug. */
#ifndef BHDLC_DEBUG
#  define BHDLC_DEBUG				0
#endif

/* Debug messages are written to stdout as default */
#ifndef BHDLC_DEBUG_PRINTF
#  define BHDLC_DEBUG_PRINTF(x)	do {printf x;} while(0)
#endif

/* bHDLC can generate some statistics on the link, mainly for errors. */
#ifndef BHDLC_STATISTICS
#  define BHDLC_STATISTICS			0
#endif

/* Maximum number of allocated links */
#ifndef BHDLC_LINKS_NUM
#  define BHDLC_LINKS_NUM			1
#endif

/* Default value of maximum number of retransmissions, before disconnecting.
 * The default value can be changed at runtime with bHDLC_set_maxretr() */
#ifndef BHDLC_RETRY_MAX
#  define BHDLC_RETRY_MAX			3
#endif

/* Default type for system ticks. */
#ifndef BHDLC_SYS_TICKS_T
#  define BHDLC_SYS_TICKS_T			unsigned long
#endif

#ifndef BHDLC_TXWINDOW_SIZE
#  define BHDLC_TXWINDOW_SIZE			3
#endif

#ifndef BHDLC_TXQUEUE_SIZE
#  define BHDLC_TXQUEUE_SIZE		BHDLC_TXWINDOW_SIZE
#endif

/* Default value of timeout for ACKs.
 * The default value can be changed at runtime with bHDLC_set_acktout() */
#ifndef BHDLC_WAITACK_TIMEOUT
#  define BHDLC_WAITACK_TIMEOUT		100		// Time unit is system ticks
#endif

#endif
