#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <windows.h>
#include <sys/time.h>
#include "bHDLC/bHDLC.h"
#include "bHDLC/sio.h"
#include "uart.h"
#include "qassert.h"

Q_DEFINE_THIS_MODULE("qassert")

size_t client_sendfile_cb(bHDLC_link *, bHDLC_Event event, const void *data, size_t size);
void client_sendfile_main(bHDLC_link *, int argc, char *argv[]);

//size_t server_echo_cb(bHDLC_link *, bHDLC_Event event, const void *data, size_t size);
//void server_echo_main(bHDLC_link *, int argc, char *argv[]);

void client_req_main(bHDLC_link *, int argc, char *argv[]);
size_t client_req_cb(bHDLC_link *, bHDLC_Event event, const void *data, size_t size);

size_t server_resp_cb(bHDLC_link *, bHDLC_Event event, const void *data, size_t size);
void server_resp_main(bHDLC_link *, int argc, char *argv[]);

size_t server_print_cb(bHDLC_link *, bHDLC_Event event, const void *data, size_t size);
void server_print_main(bHDLC_link *, int argc, char *argv[]);

static const struct {
	const char *name;
	size_t (*link_cb)(bHDLC_link *, bHDLC_Event event, const void *data, size_t size);
	void (*example_main)(bHDLC_link *, int argc, char *argv[]);
} examples_list[] = {
	{ "sendfile", client_sendfile_cb, client_sendfile_main },
	{ "print", server_print_cb, server_print_main },
//	{ "echo", server_echo_cb, server_echo_main },
	{ "creq", client_req_cb, client_req_main },
	{ "sresp", server_resp_cb, server_resp_main },
};
static const size_t num_examples = sizeof(examples_list) / sizeof(examples_list[0]);


static bHDLC_link *link;
static UART *uart;
static int uart_noise;

static unsigned char sio_rxbuf[256];

int
sio_init(sio_fd_t fd)
{
	Q_ASSERT((UART *)fd == uart);
	uart_set_buffers(uart, sio_rxbuf, sizeof(sio_rxbuf), NULL, 0);
	uart_set_noise(uart, uart_noise);
	return uart_open(uart, 38400);
}

int
sio_putc(int c, sio_fd_t fd)
{
	UART *uart = fd;
	uart_putchar(uart, (unsigned char)c);
	return 1;
}

int
sio_getc(sio_fd_t fd)
{
	UART *uart = fd;
	if (uart_rxempty(uart)) return EOF;
	return uart_getchar(uart);
}


int
main(int argc, char *argv[])
{
	const char *ex_name = NULL;
	int ex_idx = -1;
	unsigned char port = 0;
	unsigned int ack_timeout = 0;
	unsigned int max_retr = 0;
	char port_name[] = "COM11";

	while(--argc) {
		const char *arg = *++argv;
		if (!strncmp(arg, "--", 2)) {
			argc--;
			argv++;
			break;
		} else if (!strncmp(arg, "-p", 2)) {
			port = strtoul(&arg[2], NULL, 10);
		} else if (!strncmp(arg, "-e", 2)) {
			ex_name = &arg[2];
		} else if (!strncmp(arg, "-n", 2)) {
			uart_noise = strtoul(&arg[2], NULL, 10);
		} else if (!strncmp(arg, "-t", 2)) {
			ack_timeout = strtoul(&arg[2], NULL, 10);
		} else if (!strncmp(arg, "-r", 2)) {
			max_retr = strtoul(&arg[2], NULL, 10);
		}
	}
	if (ex_name == NULL) {
		fprintf(stderr, "Specify an example through -e option\n");
		exit (EXIT_FAILURE);
	}
	for (size_t i = 0; i < num_examples; i++) {
		if (!strcmp(ex_name, examples_list[i].name)) {
			ex_idx = i;
			break;
		}
	}
	if (ex_idx < 0) {
		fprintf(stderr, "Example %s doesn't exist\n", ex_name);
		exit (EXIT_FAILURE);
	}
	if (port == 0) {
		fprintf(stderr, "Syntax error (specify COM port number with -p option)\n");
		exit (EXIT_FAILURE);
	}

	snprintf(port_name, sizeof(port_name), "COM%d", port);
	uart = uart_init(port_name);
	Q_ASSERT(uart != NULL);
	link = bHDLC_add(0x00, uart, examples_list[ex_idx].link_cb);
	if (ack_timeout != 0) {
		bHDLC_set_acktout(link, ack_timeout);
	}
	if (max_retr != 0) {
		bHDLC_set_maxretr(link, max_retr);
	}

	examples_list[ex_idx].example_main(link, argc, argv);
	exit(EXIT_SUCCESS);
}

unsigned long
sys_now(void)
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return tp.tv_sec * 1000 + tp.tv_usec / 1000;
}

#ifndef Q_NASSERT
void
Q_onAssert(char const *const file, int line)
{
	fprintf(stderr, "Assert in file %s line %d\n", file, line);
	while(1) {
	}
}
#endif
